package com.misty.entity;

import java.io.Serializable;

public class MessageEntity implements Serializable {
    private String message;
    private long time ;

    public MessageEntity(String message, long time) {
        this.message = message;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
