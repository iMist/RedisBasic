package com.misty.redission;

import com.misty.entity.MessageEntity;
import com.misty.entity.User;

import org.junit.BeforeClass;
import org.junit.Test;
import org.redisson.Redisson;

import org.redisson.api.*;
import org.redisson.api.listener.MessageListener;
import org.redisson.api.listener.PatternMessageListener;
import org.redisson.config.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class RedissonStringTest {
    private static final String HOST= "127.0.0.1";
    private static final String PORT= "6379";
    private static RedissonClient redissonClient;

    @BeforeClass
    public static void config(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://"+HOST+":"+PORT);

        redissonClient = Redisson.create(config);
    }

    /**
     * 每个Redisson对象实例都会有一个与之对应的Redis数据实例，可以通过调用getName方法来取得Redis数据实例的名称（key）
     * 所有与Redis key相关的操作都归纳在RKeys这个接口里：
     */
    @Test
    public void key(){
        RMap map = redissonClient.getMap("books");
        String age  =  map.getName();
        System.out.println(age);
        RKeys keys = redissonClient.getKeys();
        Iterable<String> allKeys = keys.getKeys();
        Iterable<String> foundedKeys = keys.getKeysByPattern("user*");
        long numOfDeletedKeys = keys.delete("user", "age", "addr");
        long deletedKeysAmount = keys.deleteByPattern("user?");
        String randomKey = keys.randomKey();
        long keysAmount = keys.count();
    }

    @Test
    public void objBucket(){
        RBucket<Object> bucket = redissonClient.getBucket("User");
        User user = new User("misty",10,"hubei");
        bucket.set(user);

        User user2 = (User) bucket.get();
        user2.setAge(50);
        bucket.trySet(user2);
        //如果当前值 == 预期值，则以原子方式将该值设置为给定的更新值,返回值为当前是否成功修改
        boolean compare = bucket.compareAndSet(user, user2);
        User user3 = (User) bucket.get();
        //返回原来的值,然后将新的值设置给bucket
        User user4 = (User) bucket.getAndSet(user);
    }

    /*
    多对象的bucket操作
     */
    @Test
    public void multipleObjBucket(){
        RBuckets buckets = redissonClient.getBuckets();
        Map<String, User> map = new HashMap<String,User>();
        map.put("User1", new User("misty",10,"hubei"));
        map.put("User2", new User("xiaowu",24,"shenzhen"));
        map.put("User3", new User("小强",20,"jiangxi"));

        // 利用Redis的事务特性，同时保存所有的通用对象桶，如果任意一个通用对象桶已经存在则放弃保存其他所有数据。
        boolean success = buckets.trySet(map);
        // 同时保存全部通用对象桶,如果存在则覆盖源数据
        buckets.set(map);
        Map<String, User> loadedBuckets = buckets.get("User1", "User2", "User3");
    }

    @Test
    public void binaryStream() throws IOException {
        RBinaryStream stream = redissonClient.getBinaryStream("stream");
        byte[] content = "hello redission ".getBytes();
        //直接设置字符串
        stream.set(content);

        //输入流读取数据,可以是文件
        InputStream is = stream.getInputStream();
        byte[] readBuffer = new byte[512];
        int len = 0;
        String s = null;
        while ((len = is.read(readBuffer)) > 0){
             s = new String(readBuffer,0,len);
        }
        System.out.println(s);
        //通过输出流写入
        OutputStream os = stream.getOutputStream();
        byte[] contentToWrite = "hello redission".getBytes();
        os.write(contentToWrite);
    }

    @Test
    public void setString(){
        String key = "books";
        RBucket<Object> result = redissonClient.getBucket(key);
        if (!result.isExists()) {
            result.set("java in think", 5, TimeUnit.MINUTES);
        }
    }

    @Test
    public void getString(){
        String key = "books";
        RBucket<Object> result = redissonClient.getBucket(key);
        if (result.isExists()){
            String value = result.get().toString();
            System.out.println(value);
        }
    }

    /**
     * 整长型累加器（LongAdder）和双精度浮点累加器（DoubleAdder）
     *
     * ,通过利用客户端内置的LongAdder对象，为分布式环境下递增和递减操作提供了很高得性能。
     * 据统计其性能最高比分布式AtomicLong对象快 12000 倍。完美适用于分布式统计计量场景。
     */
    @Test
    public void Adder(){
        RLongAdder atomicLong = redissonClient.getLongAdder("count");
        atomicLong.add(12);
        atomicLong.increment();
        atomicLong.decrement();
        atomicLong.sum();
        //当不再使用整长型累加器对象的时候应该自动手动销毁，redission被关闭（shutdown）则不用手动销毁
        atomicLong.destroy();
    }
    /**
     * 原子整长形（AtomicLong）
     */
    @Test
    public void incr(){
        String key = "age";
        RAtomicLong atomicLong = redissonClient.getAtomicLong(key);
        atomicLong.set(3);
        //递增1
        atomicLong.incrementAndGet();
        //递减1
        atomicLong.decrementAndGet();

        long age = atomicLong.get();
        System.out.println("age递增1 : "+age);

        age = atomicLong.addAndGet(10);
        System.out.println("age递增10,先添加然后再获取新的值 : "+age);
        age = atomicLong.getAndAdd(10);
        System.out.println("age递增10,先获取旧值再添加新的值 : "+age);

        age = atomicLong.get();
        System.out.println("age : "+age);
    }

    /**
     * 原子双精度浮点（AtomicDouble）
     */
    @Test
    public void AtomicDouble(){
        String key = "money";
        RAtomicDouble atomicDouble = redissonClient.getAtomicDouble(key);
        atomicDouble.set(2.81);
        double money = atomicDouble.addAndGet(4.11);
        money = atomicDouble.get();
    }

    @Test
    public void topicPub(){
        RTopic topic = redissonClient.getTopic("topic");
        long clientsReceivedMessage = topic.publish(new MessageEntity("hello redis",1000L));
    }

    @Test
    public void topicSub(){
        RTopic topic = redissonClient.getTopic("topic");
        topic.addListener(MessageEntity.class, new MessageListener<MessageEntity>() {
            public void onMessage(CharSequence charSequence, MessageEntity messageEntity) {
                System.out.println("topic类型，也就是key:"+charSequence);
                System.out.println("message :"+messageEntity.getMessage());
            }
        });
    }

    /**
     *  模糊匹配topic,订阅所有满足`topic.*`表达式的话题
     */
    @Test
    public void topicSubPattern(){

        RPatternTopic topic1 = redissonClient.getPatternTopic("topic.*");
        int listenerId = topic1.addListener(MessageEntity.class, new PatternMessageListener<MessageEntity>() {

            public void onMessage(CharSequence charSequence, CharSequence charSequence1, MessageEntity messageEntity) {

            }
        });
    }


    /**
     * 基于Redis的分布式限流器（RateLimiter）可以用来在分布式环境下限制请求方的调用频率。既适用于不同Redisson实例下的多线程限流，
     * 也适用于相同Redisson实例下的多线程限流
     * 该算法不保证公平性
     */
    @Test
    public void rateLimiter(){
        RRateLimiter rateLimiter = redissonClient.getRateLimiter("MyRateLimiter");
        // 初始化
        // 最大流速 = 每1秒钟产生10个令牌
        rateLimiter.trySetRate(RateType.OVERALL, 10, 1, RateIntervalUnit.SECONDS);

        CountDownLatch latch = new CountDownLatch(2);

        rateLimiter.acquire(1);
        rateLimiter.acquire(2);
        rateLimiter.acquire(3);
        rateLimiter.acquire(3);
        //最大每秒限流 10 , acquire(num)代表增加多少次访问,tryAcquire(num)返回添加num次数之后是否限流
        boolean isablable = rateLimiter.tryAcquire(1);
        System.out.println(isablable);
        //当前已经到达了10次限流次数,无法继续叠加被限流了
        isablable = rateLimiter.tryAcquire(1);
        System.out.println(isablable);
        Thread t = new Thread(() -> {
            //在增加两次访问,支持分布式环境也支持多线程环境
            rateLimiter.acquire(2);
            // ...
        });
        t.start();
    }



}
