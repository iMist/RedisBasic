package com.misty.redission;

import com.misty.entity.User;
import org.junit.BeforeClass;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RFuture;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class RedissionMapTest {

    private static final String HOST= "127.0.0.1";
    private static final String PORT= "6379";
    private static RedissonClient redisson;

    @BeforeClass
    public static void config(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://"+HOST+":"+PORT);

        redisson = Redisson.create(config);
    }

    @Test
    public void mapTest(){
        RMap<String, User> map = redisson.getMap("maps");
        // 如果不存在则创建 存在则直接覆盖这个key对应的值
        User prevObject = map.put("user_1", new User("misty",24,"hubei"));
        // 如果不存在则创建，存在则放弃
        User currentObject = map.putIfAbsent("user_2", new User("xiaoqiang",16,"shenzhen"));
        User obj = map.remove("user_1");

        map.fastPut("user_3", new User("dagang",10,"xianning"));
        map.fastRemove("user_3");

        RFuture<User> putAsyncFuture = map.putAsync("user_4",new User("liufei",18,"wuhan"));

        RFuture<Boolean> fastPutAsyncFuture = map.fastPutAsync("user_5",new User("helian",12,"hanchuan"));

        map.fastPutAsync("user_6" , new User("pcm",30,"shenzhen"));
        map.fastRemoveAsync("user_6");
    }
}
